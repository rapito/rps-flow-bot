from enum import Enum


class Moves(Enum):
    ROCK = 0
    PAPER = 1
    SCISSORS = 2
    LIZARD = 3
    SPOCK = 4


class Results(Enum):
    WON = 0
    LOST = 1


features = [
    [Results.LOST.value, Moves.ROCK.value, Moves.SCISSORS.value, Results.LOST.value, Moves.SCISSORS.value, Moves.PAPER.value],
    [Results.LOST.value, Moves.SCISSORS.value, Moves.PAPER.value, Results.LOST.value, Moves.SCISSORS.value, Moves.SCISSORS.value],
    [Results.LOST.value, Moves.SCISSORS.value, Moves.SCISSORS.value, Results.LOST.value, Moves.ROCK.value, Moves.ROCK.value],
    [Results.LOST.value, Moves.ROCK.value, Moves.ROCK.value, Results.LOST.value, Moves.SCISSORS.value, Moves.PAPER.value],
    [Results.LOST.value, Moves.SCISSORS.value, Moves.PAPER.value, Results.WON.value, Moves.PAPER.value, Moves.SCISSORS.value],
    [Results.WON.value, Moves.PAPER.value, Moves.SCISSORS.value, Results.LOST.value, Moves.ROCK.value, Moves.SCISSORS.value],
    [Results.LOST.value, Moves.SCISSORS.value, Moves.PAPER.value, Results.WON.value, Moves.SCISSORS.value, Moves.ROCK.value],
    [Results.WON.value, Moves.SCISSORS.value, Moves.ROCK.value, Results.LOST.value, Moves.SCISSORS.value, Moves.SCISSORS.value],
    [Results.LOST.value, Moves.SCISSORS.value, Moves.SCISSORS.value, Results.LOST.value, Moves.ROCK.value, Moves.SCISSORS.value],
    [Results.LOST.value, Moves.ROCK.value, Moves.SCISSORS.value, Results.WON.value, Moves.SCISSORS.value, Moves.ROCK.value],
    [Results.WON.value, Moves.SCISSORS.value, Moves.ROCK.value, Results.LOST.value, Moves.SCISSORS.value, Moves.PAPER.value],
    [Results.LOST.value, Moves.PAPER.value, Moves.PAPER.value, Results.LOST.value, Moves.ROCK.value, Moves.SCISSORS.value]
]

extended_features = [
    [Results.LOST.value, Moves.ROCK.value, Moves.SCISSORS.value, Results.LOST.value, Moves.SCISSORS.value, Moves.PAPER.value],
    [Results.LOST.value, Moves.SCISSORS.value, Moves.PAPER.value, Results.LOST.value, Moves.SCISSORS.value, Moves.SCISSORS.value],
    [Results.LOST.value, Moves.SCISSORS.value, Moves.SCISSORS.value, Results.LOST.value, Moves.ROCK.value, Moves.ROCK.value],
    [Results.LOST.value, Moves.ROCK.value, Moves.ROCK.value, Results.LOST.value, Moves.SCISSORS.value, Moves.PAPER.value],
    [Results.LOST.value, Moves.SCISSORS.value, Moves.PAPER.value, Results.WON.value, Moves.PAPER.value, Moves.SCISSORS.value],
    [Results.WON.value, Moves.PAPER.value, Moves.SCISSORS.value, Results.LOST.value, Moves.ROCK.value, Moves.SCISSORS.value],
    [Results.LOST.value, Moves.SCISSORS.value, Moves.PAPER.value, Results.WON.value, Moves.SCISSORS.value, Moves.ROCK.value],
    [Results.WON.value, Moves.SCISSORS.value, Moves.ROCK.value, Results.LOST.value, Moves.SCISSORS.value, Moves.SCISSORS.value],
    [Results.LOST.value, Moves.SCISSORS.value, Moves.SCISSORS.value, Results.LOST.value, Moves.ROCK.value, Moves.SCISSORS.value],
    [Results.LOST.value, Moves.ROCK.value, Moves.SCISSORS.value, Results.WON.value, Moves.SCISSORS.value, Moves.ROCK.value],
    [Results.WON.value, Moves.SCISSORS.value, Moves.ROCK.value, Results.LOST.value, Moves.SCISSORS.value, Moves.PAPER.value],
    [Results.LOST.value, Moves.PAPER.value, Moves.PAPER.value, Results.LOST.value, Moves.ROCK.value, Moves.SCISSORS.value]
]

features_names = ["1st Result",
                  "Human's 1st Move",
                  "Bot's 1st Move",
                  "2nd Result",
                  "Human's 2nd Move",
                  "Bot's 2nd Move"]

labels = [
    Moves.ROCK.value,
    Moves.ROCK.value,
    Moves.PAPER.value,
    Moves.ROCK.value,
    Moves.SCISSORS.value,
    Moves.PAPER.value,
    Moves.ROCK.value,
    Moves.ROCK.value,
    Moves.PAPER.value,
    Moves.ROCK.value,
    Moves.ROCK.value,
    Moves.PAPER.value,
]

if __name__ == "__main__":
    print([e.value for e in Moves])
