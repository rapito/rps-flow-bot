from flask import Flask, render_template, make_response, Response, request, send_file, jsonify
import skl
import data
import io

app = Flask(__name__)


def get(param_name, default=None):
    res = request.args.get(param_name)
    if not res:
        res = default

    return res


def getj(param_name, default=None):
    res = request.get_json()[param_name]
    if not res:
        res = default

    return res


@app.route('/predict', methods=['POST'])
def predict_move():
    prediction, _ = skl.predict(getj("data"), getj("labels"))
    return jsonify(move=int(prediction), move_name=data.Moves(prediction).name)


@app.route('/graph', methods=['POST'])
def graph():
    prediction, graph = skl.predict(getj("data"), getj("labels"), do_graph=True)
    return send_file(io.BytesIO(graph), mimetype='image/png', as_attachment=True,
                     attachment_filename='graph.png')


@app.route('/', methods=['GET'])
def home():
    prediction, graph = skl.predict(None, None, do_graph=True)
    return send_file(io.BytesIO(graph), mimetype='image/png', as_attachment=True,
                     attachment_filename='graph.png')
