from sklearn import tree
import data

from sklearn.externals.six import StringIO
import pydot


def get_clf():
    return tree.DecisionTreeClassifier()


def train_and_predict(clf, training_data=data.features, labels=data.labels):
    past_move = training_data[-1]
    model = training_data[:-1]
    labels = labels[:-1]

    clf = clf.fit(model, labels)
    res = clf.predict([past_move])

    print("train_and_predict past move: %s" % past_move)
    print("train_and_predict model: ")
    print(model)
    print("train_and_predict labels: ")
    print(labels)

    print("train_and_predict result: %s" % data.Moves(res[0]))
    return res[0]


def graph_tree(classifier, feature_names=data.features_names):
    dot_data = StringIO()
    tree.export_graphviz(classifier, out_file=dot_data, feature_names=feature_names,
                         class_names=[e.name for e in data.Moves],
                         filled=True,
                         rounded=True, impurity=False)
    graph = pydot.graph_from_dot_data(dot_data.getvalue())
    return graph[0].create(prog="dot", format="png")


def predict(training_data, labels, do_graph=False):
    clf = get_clf()

    if training_data is None or labels is None or len(training_data) < 5:
        training_data = data.features
        labels = data.labels

    prediction = train_and_predict(clf, training_data, labels)

    graph = None
    if do_graph:
        graph = graph_tree(clf, data.features_names)

    return prediction, graph


if __name__ == "__main__":
    clf = get_clf()

    # print(data.features[:-1])
    # print(data.features[-1])
    train_and_predict(clf)
    graph_tree(clf)
